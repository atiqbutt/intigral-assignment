package com.sakurafish.exam.hls.exoplayer;

import android.app.Activity;
import android.support.multidex.MultiDexApplication;

import com.sakurafish.exam.hls.exoplayer.di.component.DaggerApplicationComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by Atiq
 */
public class MyApplicaiton extends MultiDexApplication implements HasActivityInjector {


    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();

       DaggerApplicationComponent.builder()
                .application(this).build().inject(this);

    }


    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }
}

