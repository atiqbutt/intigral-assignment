/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sakurafish.exam.hls.exoplayer;

import com.google.android.exoplayer.util.Util;

import java.util.Locale;

/**
 * Holds statically defined sample definitions.
 */
/* package */ class Samples
{

    public static class Sample
    {

        public final String name;
        public final String contentId;
        public final String provider;
        public final String uri;
        public final int type;

        public Sample(String name, String uri, int type)
        {
            this(name, name.toLowerCase(Locale.US).replaceAll("\\s", ""), "", uri, type);
        }

        public Sample(String name, String contentId, String provider, String uri, int type)
        {
            this.name = name;
            this.contentId = contentId;
            this.provider = provider;
            this.uri = uri;
            this.type = type;
        }

    }


    public static final Sample[] HLS = new Sample[]{
             new Sample("Intigral Video", "http://intigralvod1-vh.akamaihd.net/i/3rdparty/Season2017_2018/10_12_2017_Hilal_fath/Highlights/high_,256,512,768,1200,.mp4.csmil/master.m3u8", Util.TYPE_HLS)};


    private Samples()
    {
    }

}
