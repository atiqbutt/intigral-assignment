package com.sakurafish.exam.hls.exoplayer.di.module;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface IntigralApplicaitonScope {
}
