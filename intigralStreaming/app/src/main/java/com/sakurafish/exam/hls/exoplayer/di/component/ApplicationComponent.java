package com.sakurafish.exam.hls.exoplayer.di.component;

import android.app.Application;
import android.support.annotation.NonNull;


import com.sakurafish.exam.hls.exoplayer.MyApplicaiton;
import com.sakurafish.exam.hls.exoplayer.di.module.ActivityBuilderModule;
import com.sakurafish.exam.hls.exoplayer.di.module.ApplicationModule;
import com.sakurafish.exam.hls.exoplayer.di.module.IntigralApplicaitonScope;
import com.sakurafish.exam.hls.exoplayer.di.module.NetworkModule;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;


@IntigralApplicaitonScope
@Component(modules = {ApplicationModule.class, NetworkModule.class,
        AndroidInjectionModule.class,
        ActivityBuilderModule.class
})
public interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @NonNull
        @BindsInstance
        Builder application(Application application);
        @NonNull
        ApplicationComponent build();
    }
    void inject(MyApplicaiton app);
}


