package com.sakurafish.exam.hls.exoplayer.di.module;

import android.app.Application;

import com.sakurafish.exam.hls.exoplayer.di.scope.ApplicationContext;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    @Provides
    @ApplicationContext
    @IntigralApplicaitonScope
    Application providesContext(Application application)
    {
        return application;
    }



}
