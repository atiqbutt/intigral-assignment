package com.sakurafish.exam.hls.exoplayer.Base;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.Context;

import android.os.Bundle;
import android.support.annotation.LayoutRes;

import android.support.annotation.Nullable;

import android.support.v7.app.ActionBar;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import android.widget.TextView;


import com.sakurafish.exam.hls.exoplayer.data.utils.ConnectionLiveData;
import com.sakurafish.exam.hls.exoplayer.data.utils.ConnectionModel;
import com.sakurafish.exam.hls.exoplayer.data.utils.ConnectionUtils;

import dagger.android.AndroidInjection;


public abstract class BaseActivity extends AppCompatActivity  {


    @Nullable
    protected ActionBar mActionBar;



    private ProgressDialog progressDialog;
    private boolean mIsNetworkAvailable = false;
    protected Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        context = this;

        ConnectionLiveData connectionLiveData = new ConnectionLiveData(getApplicationContext());
        mIsNetworkAvailable = ConnectionUtils.INSTANCE.isNetworkAvailable(this);
        observeNetwork(connectionLiveData);
    }

    private void observeNetwork(ConnectionLiveData connectionLiveData) {
        connectionLiveData.observe(this, new Observer<ConnectionModel>() {
            @Override
            public void onChanged(@Nullable ConnectionModel connection) {
                if (connection.getIsConnected()) {
                    switch (connection.getType()) {
                        case 1:
                            mIsNetworkAvailable = true;

                          //  Toast.makeText(getApplicationContext(), String.format("Wifi turned ON"),Toast.LENGTH_SHORT).show();
                            break;
                        case 2:

                            mIsNetworkAvailable = true;
                           // Toast.makeText(getApplicationContext(), String.format("Mobile data turned ON"), Toast.LENGTH_SHORT).show();
                            break;
                    }
                } else {
                    mIsNetworkAvailable = false;

                    //Toast.makeText(getApplicationContext(), String.format("Connection turned OFF"), Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


    protected void initialiseActionBar() {
        if (null == mActionBar) {
            mActionBar = getSupportActionBar();
        }
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setHomeButtonEnabled(false);
    }

    /**
     * Check if the network is available
     *
     * @return return true if the network is available
     */
    protected boolean isNetworkAvailable() {
        return mIsNetworkAvailable;
    }


    /**
     * Public method which will get called from the child fragment as well as
     * Derived activity classes.
     *
     * @param title
     */
    public void setActionBarTitle(String title) {
        if (null != mActionBar) {
            mActionBar = getSupportActionBar();
        }
        mActionBar.setTitle(title);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      /*  getMenuInflater().inflate(R.menu.main, menu);
        MenuItem actionClose = menu.findItem(R.id.action_filter);
        actionClose.setOnMenuItemClickListener(this);
        menu.findItem(R.id.switch_view).setVisible(false).setOnMenuItemClickListener(this);
        actionClose.setVisible(false);*/
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        saveCurrentStateToBundle(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        restoreCurrentStateFromBundle(savedInstanceState);
    }

    protected void saveCurrentStateToBundle(Bundle outState) {
        //To be overridden by sub-classes, if needed
    }

    protected void restoreCurrentStateFromBundle(Bundle savedInstanceState) {
        //To be overridden by sub-classes, if needed
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
