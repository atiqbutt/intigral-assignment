package com.sakurafish.exam.hls.exoplayer.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ModelHomeTeam {

    @SerializedName("Players")
    List<Players> playersList = new ArrayList<>();

    public List<Players> getPlayersList() {
        return playersList;
    }

    public void setPlayersList(List<Players> playersList) {
        this.playersList = playersList;
    }
}
