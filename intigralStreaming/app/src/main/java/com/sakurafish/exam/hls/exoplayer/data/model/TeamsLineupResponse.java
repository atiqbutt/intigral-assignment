package com.sakurafish.exam.hls.exoplayer.data.model;

import com.google.gson.annotations.SerializedName;

public class TeamsLineupResponse {

    @SerializedName("Lineups")
    LineUps lineUps;

    public LineUps getLineUps() {
        return lineUps;
    }

    public void setLineUps(LineUps lineUps) {
        this.lineUps = lineUps;
    }


}
