package com.sakurafish.exam.hls.exoplayer;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.sakurafish.exam.hls.exoplayer.data.network.loader.PlayersLineupLoader;

import javax.inject.Inject;

public class MainActivityViewModelFactory extends ViewModelProvider.NewInstanceFactory  {
    @Inject
    PlayersLineupLoader lineupLoader;

    @Inject
    public MainActivityViewModelFactory()
    {

    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new PlayerActivityViewModel(lineupLoader);
    }
}
