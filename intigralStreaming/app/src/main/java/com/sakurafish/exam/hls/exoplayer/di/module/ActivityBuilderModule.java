package com.sakurafish.exam.hls.exoplayer.di.module;

import android.support.annotation.NonNull;


import com.sakurafish.exam.hls.exoplayer.MainActivity;
import com.sakurafish.exam.hls.exoplayer.PlayerActivity;
import com.sakurafish.exam.hls.exoplayer.di.scope.ActivityScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by atiq on 15/10/2018.
 */
@Module
public abstract class ActivityBuilderModule {


    @NonNull
    @ActivityScope
    @ContributesAndroidInjector(modules = { HandlerModule.class})
    abstract MainActivity bindMainActivity();

    @NonNull
    @ActivityScope
    @ContributesAndroidInjector(modules = {ActivityModule.class})
    abstract PlayerActivity bindPlayerActivity();




}
