package com.sakurafish.exam.hls.exoplayer;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sakurafish.exam.hls.exoplayer.data.model.Players;

import java.util.ArrayList;
import java.util.List;


public class AwayLineUpsAdapter extends RecyclerView.Adapter<AwayLineUpsAdapter.AwayLineUpsAdapterViewHolder> {


    private Context mContext;
    private int mLayoutId;
    private List<Players> playersList = new ArrayList<Players>();

    public AwayLineUpsAdapter(List<Players> playersList, int mLayoutId) {
        this.playersList = playersList;

        this.mLayoutId = mLayoutId;
    }

    @Override
    public AwayLineUpsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView;
        mItemView = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        mContext = parent.getContext();
        return new AwayLineUpsAdapterViewHolder(mItemView);
    }

    @Override
    public void onBindViewHolder(AwayLineUpsAdapterViewHolder holder, int position) {

        Players item = playersList.get(position);
        holder.textView_player_name.setText(item.getName());
        holder.textView_player_role.setText(item.getRole());


    }

    @Override
    public int getItemCount() {
        return playersList.size();
    }

    public class AwayLineUpsAdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView textView_player_name, textView_player_role;

        public AwayLineUpsAdapterViewHolder(View mView) {
            super(mView);
            textView_player_name = mView.findViewById(R.id.textView_player_name);
            textView_player_role = mView.findViewById(R.id.textView_player_role);

        }
    }







}

