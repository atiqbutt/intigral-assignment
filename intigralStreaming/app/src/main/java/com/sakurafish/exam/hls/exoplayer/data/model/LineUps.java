package com.sakurafish.exam.hls.exoplayer.data.model;

import com.google.gson.annotations.SerializedName;

public class LineUps {

    @SerializedName("Success")
    boolean success ;
    @SerializedName("Data")
    ModelDataTeams modelDataTeams;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ModelDataTeams getModelDataTeams() {
        return modelDataTeams;
    }

    public void setModelDataTeams(ModelDataTeams modelDataTeams) {
        this.modelDataTeams = modelDataTeams;
    }
}
