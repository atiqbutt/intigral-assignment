package com.sakurafish.exam.hls.exoplayer;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.sakurafish.exam.hls.exoplayer.Base.BaseActivity;
import com.sakurafish.exam.hls.exoplayer.data.model.ModelDataTeams;
import com.sakurafish.exam.hls.exoplayer.data.model.TeamsLineupResponse;
import com.sakurafish.exam.hls.exoplayer.data.network.response.Resource;
import com.sakurafish.exam.hls.exoplayer.data.network.response.Status;

import javax.inject.Inject;

public class MainActivity extends BaseActivity
{

    PlayerActivityViewModel mViewModel;
    @Inject
    MainActivityViewModelFactory viewModelFactory;
    HomeLineUpsAdapter homeLineUpsAdapter;
    AwayLineUpsAdapter awayLineUpsAdapter;
    RecyclerView recyclerView_home;
    RecyclerView recyclerView_away;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView_home = findViewById(R.id.recyler_home_team);
        recyclerView_away = findViewById(R.id.recyler_away_team);

        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(PlayerActivityViewModel.class);
        observePlayersLinupsData();
    }

    private void onSampleSelected(Samples.Sample sample)
    {
        Intent mpdIntent = new Intent(this, PlayerActivity.class).setData(Uri.parse(sample.uri)).putExtra(PlayerActivity.CONTENT_ID_EXTRA, sample.contentId)
                .putExtra(PlayerActivity.CONTENT_TYPE_EXTRA, sample.type).putExtra(PlayerActivity.PROVIDER_EXTRA, sample.provider);
        startActivity(mpdIntent);
    }

    public void onClick5(View view)
    {
        onSampleSelected(Samples.HLS[0]);
    }

    private void populateLineUps(Resource<TeamsLineupResponse> userResource){
        ModelDataTeams modelDataTeams = userResource.getData().getLineUps().getModelDataTeams();

        homeLineUpsAdapter = new HomeLineUpsAdapter(modelDataTeams.getModelHomeTeam().getPlayersList(), R.layout.lineup_item);
        awayLineUpsAdapter = new AwayLineUpsAdapter(modelDataTeams.getModelAwayTeam().getPlayersList(),R.layout.lineup_item);
        recyclerView_home.setAdapter(homeLineUpsAdapter);
        recyclerView_away.setAdapter(awayLineUpsAdapter);

    }


    /**
     * Get players-lineup data from server
     *
     */
    void observePlayersLinupsData() {




        mViewModel.getPlayersLinups().observe(this, userResource -> {

            if (userResource != null &&
                    userResource.getData() != null && userResource.getStatus().equals(Status.SUCCESS)) {

            Log.d("response", userResource.getData().getLineUps().getModelDataTeams().getModelHomeTeam().getPlayersList().get(0).getName()+"");

                    populateLineUps(userResource);


            } else if (userResource != null && userResource.getStatus().equals(Status.ERROR)) {
                Log.e("ERROR","Failed to get pass details from server");

            }
        });
    }



}
