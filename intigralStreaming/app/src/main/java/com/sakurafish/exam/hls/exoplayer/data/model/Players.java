package com.sakurafish.exam.hls.exoplayer.data.model;

import com.google.gson.annotations.SerializedName;

public class Players {
    @SerializedName("Order")
    int order;
    @SerializedName("StartInField")
    boolean startInField;
    @SerializedName("Role")
    String role;
    @SerializedName("IsCaptain")
    boolean isCaptain;
    @SerializedName("JerseyNumber")
    int jerseyNumber;
    @SerializedName("Id")
    int id;
    @SerializedName("Name")
    String name;
    @SerializedName("XCoordinate")
    String xCoordinate;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isStartInField() {
        return startInField;
    }

    public void setStartInField(boolean startInField) {
        this.startInField = startInField;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isCaptain() {
        return isCaptain;
    }

    public void setCaptain(boolean captain) {
        isCaptain = captain;
    }

    public int getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(String xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public String getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(String yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    @SerializedName("YCoordinate")

    String yCoordinate;


}
