package com.sakurafish.exam.hls.exoplayer.data.model;

import com.google.gson.annotations.SerializedName;

public class ModelDataTeams {

    @SerializedName("HomeTeam")
    private ModelHomeTeam modelHomeTeam;
    @SerializedName("AwayTeam")
    private ModelAwayTeam modelAwayTeam;

    public ModelHomeTeam getModelHomeTeam() {
        return modelHomeTeam;
    }

    public void setModelHomeTeam(ModelHomeTeam modelHomeTeam) {
        this.modelHomeTeam = modelHomeTeam;
    }

    public ModelAwayTeam getModelAwayTeam() {
        return modelAwayTeam;
    }

    public void setModelAwayTeam(ModelAwayTeam modelAwayTeam) {
        this.modelAwayTeam = modelAwayTeam;
    }
}
