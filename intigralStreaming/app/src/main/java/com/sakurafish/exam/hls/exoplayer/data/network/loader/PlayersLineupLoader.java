package com.sakurafish.exam.hls.exoplayer.data.network.loader;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.sakurafish.exam.hls.exoplayer.data.AbsentLiveData;
import com.sakurafish.exam.hls.exoplayer.data.model.TeamsLineupResponse;
import com.sakurafish.exam.hls.exoplayer.data.network.response.ApiResponse;
import com.sakurafish.exam.hls.exoplayer.data.network.response.Resource;
import com.sakurafish.exam.hls.exoplayer.data.resource.NetworkBoundResource;

import javax.inject.Inject;

public class PlayersLineupLoader extends Loader<Resource<TeamsLineupResponse>>{

    @Inject
    PlayersLineupLoader() {}


    @Override
    public LiveData<Resource<TeamsLineupResponse>> load() {
        return new NetworkBoundResource<TeamsLineupResponse, TeamsLineupResponse>(NetworkBoundResource.LOAD_FIRST,
                NetworkBoundResource.BOUND_FROM_BACKEND) {
            @Override
            protected void saveToCache(@NonNull TeamsLineupResponse players) {

            }

            @Override
            protected boolean shouldFetch(@Nullable TeamsLineupResponse players) {
                return players == null;
            }

            @NonNull
            @Override
            protected LiveData<TeamsLineupResponse> loadFromCache() {
                return AbsentLiveData.create();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<TeamsLineupResponse>> loadFromNetwork() {
                return playerService.getTeamsLineup();
            }

            @Override
            protected void clearCache() {

            }
        }.asLiveData();
    }


}
