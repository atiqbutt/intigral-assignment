package com.sakurafish.exam.hls.exoplayer.data.resource;/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Copyright (C) 2017 Lukoh Nam, goForer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.sakurafish.exam.hls.exoplayer.data.AbsentLiveData;
import com.sakurafish.exam.hls.exoplayer.data.network.response.ApiResponse;
import com.sakurafish.exam.hls.exoplayer.data.network.response.Resource;


/**
 * A generic class that can provide a resource backed by the network.
 * <p>
 * You can read more about it in the <a href="https://developer.android.com/arch">Architecture
 * Guide</a>.
 * @param <ResultType>
 */
public abstract class NetworkBoundResource<ResultType, RequestType> {
    @SuppressWarnings("unused")
    public static final int BOUND_TO_BACKEND = 0;
    public static final int BOUND_FROM_BACKEND = 1;

    public static final int LOAD_FIRST = 0;
    public static final int LOAD_NEXT = 1;
    // This LOAD_UPDATE is not used in this AppArchitecture project.
    // But in case of updating com.maqta.android.app.epass.data from back-end server, this static variable could be used.
    public static final int LOAD_UPDATE = 2;

    private final MediatorLiveData<Resource<ResultType>> mResult = new MediatorLiveData<>();

    @MainThread
    protected NetworkBoundResource(int loadType, int boundType) {
        if (boundType == BOUND_FROM_BACKEND) {
            mResult.setValue(Resource.loading(null));
            LiveData<ResultType> cacheSource;
            switch (loadType) {
                case LOAD_FIRST:
                    cacheSource = loadFromCache();
                    break;
                case LOAD_NEXT:
                case LOAD_UPDATE: //To Do::Implement the code of updating in the future
                default:
                    cacheSource = AbsentLiveData.create();
                    break;
            }

            mResult.addSource(cacheSource, new Observer<ResultType>() {
                @Override
                public void onChanged(@Nullable ResultType data) {
                    mResult.removeSource(cacheSource);
                    if (NetworkBoundResource.this.shouldFetch(data)) {
                        NetworkBoundResource.this.fetchFromNetwork(cacheSource, loadType);
                    } else {
                        mResult.addSource(cacheSource, new Observer<ResultType>() {
                            @Override
                            public void onChanged(@Nullable ResultType updatedData) {
                                mResult.setValue(
                                        Resource.success(updatedData));
                            }
                        });
                    }
                }
            });
        } else {
            // TO BE::Implemented code to send something
        }
    }

    private void fetchFromNetwork(@NonNull final LiveData<ResultType> cacheSource, int loadType) {
        LiveData<ApiResponse<RequestType>> apiResponse = loadFromNetwork();
        // we re-attach cacheSource as a new source, it will dispatch its latest value quickly
        mResult.addSource(cacheSource, new Observer<ResultType>() {
            @Override
            public void onChanged(@Nullable ResultType newData) {
                mResult.setValue(Resource.loading(newData));
            }
        });
        mResult.addSource(apiResponse, new Observer<ApiResponse<RequestType>>() {
            @Override
            public void onChanged(@Nullable ApiResponse<RequestType> response) {
                mResult.removeSource(apiResponse);
                mResult.removeSource(cacheSource);
                //no inspection ConstantConditions
                if (response != null && response.isSuccessful()) {

                    ResultType resultTypeLiveData = (ResultType) response.body;
                    //   saveResultAndReInit(response, loadType);
                    mResult.setValue(Resource.success(resultTypeLiveData));

                } else {
                    NetworkBoundResource.this.onFetchFailed();
                    mResult.addSource(cacheSource,
                            new Observer<ResultType>() {
                                @Override
                                public void onChanged(@Nullable ResultType newData) {
                                    mResult.setValue(Resource.error(response != null
                                            ? response.errorMessage : null, newData));
                                }
                            });
                }

            }
        });




    }


    protected void onFetchFailed() {
    }

    @NonNull
    public LiveData<Resource<ResultType>> asLiveData() {
        return mResult;
    }

    @WorkerThread
    protected abstract void saveToCache(@NonNull RequestType item);

    @MainThread
    protected abstract boolean shouldFetch(@Nullable ResultType data);

    @NonNull
    @MainThread
    protected abstract LiveData<ResultType> loadFromCache();

    @NonNull
    @MainThread
    protected abstract LiveData<ApiResponse<RequestType>> loadFromNetwork();

    protected abstract void clearCache();
}
