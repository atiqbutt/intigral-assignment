package com.sakurafish.exam.hls.exoplayer.di.module;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sakurafish.exam.hls.exoplayer.Constants;
import com.sakurafish.exam.hls.exoplayer.data.factory.LiveDataCallAdapterFactory;
import com.sakurafish.exam.hls.exoplayer.data.network.api.PlayerService;
import com.sakurafish.exam.hls.exoplayer.data.utils.ConnectionUtils;
import com.sakurafish.exam.hls.exoplayer.di.scope.AuthenticatorIntercept;
import com.sakurafish.exam.hls.exoplayer.di.scope.OkHttpAuth;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @NonNull
    @Provides
    @IntigralApplicaitonScope
    Cache provieHttpCache(@NonNull Application application) {
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @NonNull
    @Provides
    @IntigralApplicaitonScope
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @NonNull
    @Provides
    @IntigralApplicaitonScope
    HttpLoggingInterceptor providehttpLoggingInterceptor() {
        return new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("network logg = ", message);
            }
        });
    }

    @NonNull
    @Provides
    @IntigralApplicaitonScope
    @AuthenticatorIntercept
    Interceptor provideBasicAuthenticatorInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request authenticatedRequest = request.newBuilder()
                .header("Content-Type", "application/json")
                        .header("Authorization", Credentials.basic("osbtest","N3wY0rK2o17")).build();
                return chain.proceed(authenticatedRequest);
            }
        };
    }


    @NonNull
    @Provides
    @IntigralApplicaitonScope
    @OkHttpAuth
    OkHttpClient provideOkhttpClientWithAuth(Cache cache, @NonNull HttpLoggingInterceptor httpLoggingInterceptor,
                                     @AuthenticatorIntercept Interceptor BasicAuthenticatorInterceptor) {
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        client.addInterceptor(httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY));
          client.addInterceptor(BasicAuthenticatorInterceptor);
        client.cache(cache);
        return client.build();
    }



    @NonNull
    @Provides
    @IntigralApplicaitonScope
    OkHttpClient provideOkhttpClient(Cache cache, @NonNull HttpLoggingInterceptor httpLoggingInterceptor,
                                     @NonNull Interceptor OfflineCacheInterceptor) {
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        client.addInterceptor(httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY));
      //  client.addInterceptor(OfflineCacheInterceptor);
        client.cache(cache);
        return client.build();
    }

    @NonNull
    @IntigralApplicaitonScope
    @Provides
    Interceptor providesOfflineCacheInterceptor(@NonNull Application application) {

        return new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {

                Request request = chain.request();

                if (!ConnectionUtils.INSTANCE.isNetworkAvailable(application)) {

                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale(7, TimeUnit.DAYS)
                            .build();

                    request = request.newBuilder().
                            cacheControl(cacheControl).build();
                }
                return chain.proceed(request);
            }
        };
    }



    @Provides
    @IntigralApplicaitonScope
    PlayerService providePassRetrofit(@NonNull Gson gson, @NonNull @OkHttpAuth OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build().create(PlayerService.class);
    }


}
