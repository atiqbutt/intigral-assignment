package com.sakurafish.exam.hls.exoplayer.di.module;


import android.content.Intent;

import com.sakurafish.exam.hls.exoplayer.MainActivity;
import com.sakurafish.exam.hls.exoplayer.PlayerActivity;
import com.sakurafish.exam.hls.exoplayer.di.scope.ActivityScope;
import com.sakurafish.exam.hls.exoplayer.di.scope.MainActivityIntent;
import com.sakurafish.exam.hls.exoplayer.di.scope.PlayerActivityIntent;

import dagger.Module;
import dagger.Provides;

/**
 *  Creaated by atiq on   15/10/18.
 */
@Module
public  class ActivityModule {



    @Provides
    @ActivityScope
    @MainActivityIntent
    Intent providesMainActivityIntent(MainActivity context ) {
        return   new Intent(context, MainActivity.class);
    }

    @Provides
    @ActivityScope
    @PlayerActivityIntent
    Intent providesSmartCardIntent(PlayerActivity context ) {
        return   new Intent(context, PlayerActivity.class);
    }



}




