package com.sakurafish.exam.hls.exoplayer.data.network.api;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.sakurafish.exam.hls.exoplayer.data.model.TeamsLineupResponse;
import com.sakurafish.exam.hls.exoplayer.data.network.response.ApiResponse;

import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PlayerService {

    @GET("http://devapi.dawriplus.com/sample.json")
    LiveData<ApiResponse<TeamsLineupResponse>> getTeamsLineup();
}
