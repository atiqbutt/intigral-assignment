package com.sakurafish.exam.hls.exoplayer;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import com.sakurafish.exam.hls.exoplayer.data.model.TeamsLineupResponse;
import com.sakurafish.exam.hls.exoplayer.data.network.loader.PlayersLineupLoader;
import com.sakurafish.exam.hls.exoplayer.data.network.response.Resource;

public class PlayerActivityViewModel extends ViewModel {

    public PlayersLineupLoader playersLineupLoader;



    public PlayerActivityViewModel(PlayersLineupLoader playersLineupLoader) {
        this.playersLineupLoader = playersLineupLoader;

    }


    /**
     *get teams lineups
     * @return
     */
    public LiveData<Resource<TeamsLineupResponse>> getPlayersLinups() {
        return   playersLineupLoader.load();
    }


}
